Feature: Main Menu
  Scenario: go to play menu
    Given I launched the game
    When I hit the button "Play"
    Then I should get redirected to the menu "Play"
  Scenario: go to options menu
    Given I launched the game
    When I hit the button "Options"
    Then I should get redirected to the menu "Options"
  Scenario: exit game
    Given I launched the game
    When I hit the button "Exit"
    Then the game should close itself