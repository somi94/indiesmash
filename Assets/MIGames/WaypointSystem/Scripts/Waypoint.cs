﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace MIGames.WaypointSystem {
	public enum CurveMode {Direct,Smoothed/*,Bezier*/}
	public enum AccelerationMode {Abruptly,Interpolate}
	public enum RotationMode {None,Abruptly,Interpolate}

	public class Waypoint : MonoBehaviour {
		private static int subdivisionsPerUnit = 1;
		
		public Path path {get{return GetComponentInParent<Path>();}}

		public Waypoint child {get{return _child;}}
		[HideInInspector]
		[SerializeField]
		private Waypoint _child;

		public Waypoint parent {get{return _parent;}}
		[HideInInspector]
		[SerializeField]
		private Waypoint _parent;

		public CurveMode curveMode = CurveMode.Direct;
		public AccelerationMode accelerationMode = AccelerationMode.Interpolate;
		public RotationMode rotationMode = RotationMode.Interpolate;
		public float speedModifier = 1f;
		public float idle = 0f;

		void OnDrawGizmos() {
			Gizmos.DrawSphere(this.transform.position,0.5f);

			if(this._child != null) {
				Vector3 start = this.transform.position;
				Vector3 end = this._child.transform.position;

				float distance = Vector3.Distance(start,end);
				int numberOfSubdivisions = Mathf.Max(1,(int)((float)Waypoint.subdivisionsPerUnit * distance));
				Vector3[] array = new Vector3[numberOfSubdivisions + 1];	
				for(int i = 0; i <= numberOfSubdivisions; i++) {
					float t = i / (float)numberOfSubdivisions;
					
					array[i] = CalcPosition(t);
					if(i > 0){
						Gizmos.DrawLine(array[i-1],array[i]);
					}
				}
			}
		}

		public float CalcLength() {
			if(this.child == null) {
				return 0;
			}

			if(this.curveMode == CurveMode.Direct) {
				return Vector3.Distance(this.transform.position,child.transform.position);
			}

			Vector3 start = this.transform.position;
			Vector3 end = this._child.transform.position;
			float distance = Vector3.Distance(start,end);
			int numberOfSubdivisions = Mathf.Max (1, (int)((float)Waypoint.subdivisionsPerUnit * distance));
			
			float length = 0;
			for(int i = 0; i <= numberOfSubdivisions; i++){
				if(i > 0) {
					length += Vector3.Distance(
						CalcPosition((i - 1) / (float)numberOfSubdivisions),
						CalcPosition(i / (float)numberOfSubdivisions)
					);
				}
			}

			return length;
		}

		/*private float CalcBezierLength() {
			Vector3 start = this.transform.position;
			Vector3 end = this._child.transform.position;
			float distance = Vector3.Distance(start,end);
			int numberOfSubdivisions = Mathf.Max (1, (int)((float)Waypoint.subdivisionsPerUnit * distance));

			float length = 0;
			for(int i = 0; i <= numberOfSubdivisions; i++){
				float t = i / (float)numberOfSubdivisions;

				if(i > 0) {
					length += Vector3.Distance(
						CalcBezier((i - 1) / (float)numberOfSubdivisions),
						CalcBezier(i / (float)numberOfSubdivisions)
					);
				}
			}

			return length;
		}*/
		
		public float CalcSpeedModifier(float t) {
			float speedModifier = this.speedModifier;
			if(accelerationMode == AccelerationMode.Interpolate && this.child != null) {
				speedModifier = Mathf.Lerp(this.speedModifier,this.child.speedModifier,t);
			}

			return speedModifier;
		}
		
		public Vector3 CalcPosition(float t) {
			if(this.child == null) {
				return this.transform.position;
			}
			
			/*if(curveMode == CurveMode.Bezier) {
				return CalcBezier(t);
			}*/
			if(curveMode == CurveMode.Smoothed) {
				return CalcSmoothedPosition(t);
			} else {
				return CalcDirectPosition(t);
			}
		}

		private Vector3 CalcDirectPosition(float t) {
			Vector3 startPoint = this.transform.position;
			if(this.parent != null) {
				startPoint = this.parent.CalcPosition(1f);
			}

			return Vector3.Lerp(startPoint, child.transform.position, t);
		}

		private Vector3 CalcSmoothedPosition(float t) {
			Vector3 startPoint = this.transform.position;
			if(this.parent != null) {
				startPoint = this.parent.CalcPosition(1f);
			}
			float distance = Vector3.Distance(startPoint,this.child.transform.position);

			Vector3 toNext = Vector3.Lerp(startPoint, child.transform.position, t);
			
			if(this.parent != null) {
				Vector3 parentPosition = this.parent.CalcPosition(0.9f);
				Vector3 fromParent = Vector3.Lerp(startPoint, startPoint + (startPoint - parentPosition).normalized * distance, t);
				toNext = Vector3.Lerp(fromParent,toNext,Mathf.Min(1,t));			
			}

			if(this.child.child == null) {
				return toNext;
			}

			Vector3 toAfterNext = Vector3.Lerp(startPoint, child.child.transform.position, t);

			return Vector3.Lerp(toNext,toAfterNext,t * 0.25f);
		}

		/*private Vector3 CalcBezier(float t) {
			// B(t) = (1-t)^3 * startPosition + 3 * (1-t)^2 * t * startTangent + 3 * (1-t) * t^2 * endTangent + t^3 * endPosition, t=[0,1]
			Vector3 start = this.transform.position;
			Vector3 end = this._child.transform.position;
			float distance = Vector3.Distance(start,end);
			float bezierControlLength = distance * 0.25f;
			
			Vector3 startTangent = new Vector3();
			Vector3 endTangent = new Vector3();
			if(_parent != null) {
				startTangent = start + (start - _parent.transform.position).normalized * bezierControlLength;
			}
			if(_child.child != null) {
				endTangent = end - (_child.child.transform.position - end).normalized * bezierControlLength;
			}

			float omt = 1.0f - t;
			
			return 
				start * (omt * omt * omt) + 
				startTangent * (3 * omt * omt * t) + 
				endTangent * (3 * omt * t * t) +
				end * (t * t * t)
			;
		}*/

		public void SetChild(Waypoint child) {
			this._child = child;
			this._child._parent = this;
			if(child.path != this.path) {
				this._child.transform.SetParent(this._parent.transform,true);
			}
		}

		public static Waypoint CreateWaypoint(string name,Path path) {
			return CreateWaypoint(name,path,null);
		}

		public static Waypoint CreateWaypoint(string name,Path path,Waypoint parent) {
			GameObject waypointObj = new GameObject(name);
			
			GameObjectUtility.SetParentAndAlign(waypointObj,path.gameObject);
			
			Waypoint waypoint = waypointObj.AddComponent<Waypoint>();
			if(parent != null) {
				parent.SetChild(waypoint);

				waypoint.curveMode = parent.curveMode;
				waypoint.accelerationMode = parent.accelerationMode;
				waypoint.rotationMode = parent.rotationMode;
				waypoint.speedModifier = parent.speedModifier;
				waypoint.transform.position = parent.transform.position;
				waypoint.transform.eulerAngles = parent.transform.eulerAngles;
			}

			return waypoint;
		}
	}
}
