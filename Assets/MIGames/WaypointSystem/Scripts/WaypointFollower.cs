﻿using UnityEngine;
using System.Collections;

namespace MIGames.WaypointSystem {
	[RequireComponent(typeof(Rigidbody))]
	public class WaypointFollower : MonoBehaviour {
		private new Rigidbody rigidbody;

		public Path currentPath;
		public float speed = 1f;
		public float rotationSpeed = 90f;

		private Waypoint lastWaypoint;
		private Waypoint nextWaypoint;
		private float stepCounter;
		private float wayLength;
		private float idle = 0f;

		void Awake() {
			this.rigidbody = GetComponent<Rigidbody>();
		}

		void Update() {
			if(IsOnPath()) {
				if(idle > 0) {
					idle -= Time.deltaTime;
					return;
				}

				float step = Time.deltaTime * CalcCurrentSpeed();

				if(this.stepCounter + step > this.wayLength) {
					this.lastWaypoint = this.nextWaypoint;
					this.nextWaypoint = this.nextWaypoint.child;
					this.stepCounter = this.stepCounter - this.wayLength;
					this.wayLength = this.lastWaypoint.CalcLength();
					this.idle = this.lastWaypoint.idle;

					SendMessage("OnWaypointReached",this.lastWaypoint,SendMessageOptions.DontRequireReceiver);
				}

				this.stepCounter += step;

				float distanceCovered = this.stepCounter / this.wayLength;

				Vector3 position = lastWaypoint.CalcPosition(distanceCovered);
				Vector3 lookAt = lastWaypoint.CalcPosition(distanceCovered + 0.1f);
				Quaternion lookRotation = Quaternion.LookRotation(lookAt - position);

				this.rigidbody.MovePosition(position);
				if(lastWaypoint.rotationMode == RotationMode.Abruptly) {
					this.rigidbody.MoveRotation(lookRotation);
				} else if(lastWaypoint.rotationMode == RotationMode.Interpolate) {
					this.rigidbody.MoveRotation(Quaternion.RotateTowards(this.transform.rotation,lookRotation,rotationSpeed * Time.deltaTime));
				}
			}
		}
		
		public bool IsOnPath() {
			return this.currentPath != null && this.lastWaypoint != null && this.nextWaypoint != null;
		}
		
		public bool IsIdling() {
			return this.idle > 0f;
		}
		
		public float CalcCurrentSpeed() {
			return this.speed * CalcCurrentSpeedModifier();
		}

		public float CalcCurrentSpeedModifier() {
			return this.lastWaypoint.CalcSpeedModifier(this.stepCounter / this.wayLength);
		}
		
		public void StartPath() {
			Waypoint start = currentPath.root;
			
			this.transform.position = start.transform.position;
			this.transform.eulerAngles = start.transform.eulerAngles;
			
			this.lastWaypoint = start;
			this.nextWaypoint = start.child;
			this.stepCounter = 0f;
			this.idle = 0f;
			this.wayLength = start.CalcLength();
		}
	}
}