﻿using UnityEngine;
using UnityEditor;
using System.Collections;

namespace MIGames.WaypointSystem {
	[ExecuteInEditMode]
	public class Path : MonoBehaviour {
		public Waypoint root {get{return _root;}}
		[SerializeField]
		private Waypoint _root;

		void Start() {
			Validate();
		}

		void OnValidate() {
			Validate();
		}

		private void Validate() {
			if(_root == null) {
				_root = Waypoint.CreateWaypoint("root",this);
			}
		}
		
		[MenuItem("GameObject/Waypoint System/Path", false, 10)]
		static void DoSomething (MenuCommand menuCommand) {
			GameObject parent = (GameObject)menuCommand.context;
			
			Path path = CreatePath("New Path", parent);

			// Register the creation in the undo system
			Undo.RegisterCreatedObjectUndo(path.gameObject, "Create " + path.name);
			Selection.activeObject = path.gameObject;
		}

		public static Path CreatePath(string name,GameObject parent) {
			GameObject pathObj = new GameObject(name);
			
			Path path = pathObj.AddComponent<Path>();
			
			GameObjectUtility.SetParentAndAlign(pathObj,parent);
			
			return path;
		}
	}
}