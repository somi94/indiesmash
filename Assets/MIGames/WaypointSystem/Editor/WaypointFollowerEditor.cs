﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using MIGames.WaypointSystem;

[CustomEditor(typeof(WaypointFollower))]
public class WaypointFollowerEditor : Editor {
	public override void OnInspectorGUI() {
		DrawDefaultInspector();

		WaypointFollower follower = (WaypointFollower)target;
		
		if(follower.currentPath != null) {
			if(GUILayout.Button("Start Path")) {
				follower.StartPath();
			}
		}
	}
}
