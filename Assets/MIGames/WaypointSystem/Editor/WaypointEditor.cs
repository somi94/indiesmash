﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using MIGames.WaypointSystem;

[CustomEditor(typeof(Waypoint))]
[CanEditMultipleObjects]
public class WaypointEditor : Editor {
	public override void OnInspectorGUI() {
		DrawDefaultInspector();

		Waypoint waypoint = (Waypoint)target;

		if(waypoint.child == null) {
			if(GUILayout.Button("Add Child")) {
				Waypoint newWaypoint = Waypoint.CreateWaypoint("New Waypoint",waypoint.path,waypoint);

				Selection.activeObject = newWaypoint.gameObject;
			}
		}

		/*myTarget.experience = EditorGUILayout.IntField("Experience", myTarget.experience);

		EditorGUILayout.LabelField("Level", myTarget.Level.ToString());*/
	}
}
