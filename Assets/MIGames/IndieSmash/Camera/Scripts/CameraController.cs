using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MIGames.IndieSmash.Characters;
using MIGames.IndieSmash.Levels;

namespace MIGames.IndieSmash.Camera {
	public class CameraController : MonoBehaviour {
		public float accelerationSpeed = 5f;

		private float shakeDuration;
		private float shakeIntensity;
		private float shakeFrequency;

		private Level level;

		private Vector3 targetPosition = Vector3.zero;

		// Use this for initialization
		void Start () {
			//players = GameObject.FindObjectsOfType<Player>();
			level = GameObject.FindObjectOfType<Level>();
		}
		
		// Update is called once per frame
		void FixedUpdate () {
			float deltaTime = Time.fixedDeltaTime;
			if (shakeDuration > 0) {
				transform.position = new Vector3(transform.position.x + Mathf.Sin(Time.realtimeSinceStartup*shakeFrequency) * shakeIntensity, transform.position.y, transform.position.z);
				shakeDuration = Mathf.Clamp(shakeDuration - deltaTime, 0, 2);
			} else {
				Bounds bounds = level.boundings.GetBounds();
				Vector3 middle = bounds.center;
				float distance = bounds.extents.z;

				Player[] playersInBounds = level.boundings.GetPlayersInBounds();
				if(playersInBounds.Length > 0) {
					Vector3 max = Vector3.one * -1000;
					Vector3 min = Vector3.one * 1000;

					foreach(Player player in playersInBounds) {
						Vector3 playerPosition = player.transform.position;
						if (!level.boundings.Contains(playerPosition)) {
							continue;
						}
						playerPosition = (playerPosition + Vector3.up * 2 + (player.GetComponent<Rigidbody>().velocity / 4f));
						//middle += (playerPosition + Vector3.up + (player.GetComponent<Rigidbody>().velocity / 4f)) / (float)playersInBounds.Length;
						max = Vector3.Max(playerPosition, max);
						min = Vector3.Min(playerPosition, min);
					}	
					
					distance = Mathf.Max(Vector3.Distance(max, min), bounds.extents.z);
					middle = Vector3.Lerp(min,max,0.5f);
				}

				targetPosition = new Vector3(middle.x, middle.y, -distance);
				//targetPosition = new Vector3(Mathf.Clamp(targetPosition.x,bounds.min.x,bounds.max.x),Mathf.Clamp(targetPosition.y,bounds.min.y,bounds.max.y),targetPosition.z);
				targetPosition = new Vector3(Mathf.Clamp(targetPosition.x,bounds.min.x + distance,bounds.max.x - distance),Mathf.Clamp(targetPosition.y,bounds.min.y + distance,bounds.max.y - distance),targetPosition.z);
				transform.position = Vector3.Lerp(transform.position, targetPosition, deltaTime * accelerationSpeed);
			}
		}

		/**
		 * shakes camera
		 * 
		 * default duration: 0.25s
		 * default intensity: 0.3
		 * default frequency: 50
		 */
		public void Shake() {
			this.Shake(0.25f);
		}
		
		
		/**
		 * shakes camera
		 * 
		 * default duration: 0.25s
		 * default intensity: 0.3
		 * default frequency: 50
		 */
		public void Shake(float duration) {
			this.Shake(duration,0.35f);
		}
		
		/**
		 * shakes camera
		 * 
		 * default duration: 0.25s
		 * default intensity: 0.3
		 * default frequency: 50
		 */
		public void Shake(float duration,float intensity) {
			this.Shake(duration,intensity,50);
		}
		
		/**
		 * shakes camera
		 * 
		 * default duration: 0.25s
		 * default intensity: 0.3
		 * default frequency: 50
		 */
		public void Shake(float duration,float intensity,float frequency) {
			this.shakeDuration = duration;
			this.shakeIntensity = intensity;
			this.shakeFrequency = frequency;
		}
	}
}