﻿using UnityEngine;
using System.Collections;
using MIGames.IndieSmash.Characters.Util;

namespace MIGames.IndieSmash.Characters {
	public class HitController : MonoBehaviour {
		public float baseDefense = 1f;

		private float defenseMultiplier = 1f;
		private int hitPoints;
		private bool invulnerable = false;

		void OnSpawnStart() {
			this.hitPoints = 0;
			this.invulnerable = true;
		}

		void OnSpawnEnd() {
			this.invulnerable = false;
		}

		void OnHit(Hit hit) {
			if(this.invulnerable) {
				return;
			}

			float defense = this.baseDefense * defenseMultiplier;
			int damage = (int)((float)hit.damage / defense);

			this.hitPoints += damage;

			this.SendMessage("OnDodge",SendMessageOptions.DontRequireReceiver);

			//ToDo: calculate knockback power and directions and send it to gameobject
			//this.SendMessage("OnKnockback",new Knockback(direction,power),SendMessageOptions.DontRequireReceiver);
		}
	}
}