﻿using UnityEngine;
using System;
using System.Collections;
using MIGames.IndieSmash.Characters.Util;

namespace MIGames.IndieSmash.Characters {
	[RequireComponent(typeof(Rigidbody))]
	[RequireComponent(typeof(CapsuleCollider))]
	[RequireComponent(typeof(Animator))]
	public class MovementController : MonoBehaviour {
		public Transform rightHand;

		public int maxJumpCount = 2;
		public Vector3 jumpPower = new Vector3(3f,5f);
		public float moveSpeed = 1f;
		public Vector2 maxAirControl = new Vector2(1,1);

		private new Rigidbody rigidbody;
		private new CapsuleCollider collider;
		private Animator animator;
		
		private CharacterCommandSet commands = new CharacterCommandSet();

		private bool isSpawning = false;
		private bool knockback = false;
		private int jumpCount = 0;
		private bool isGrounded = false;
		private Vector3 movement = new Vector3();
		private bool onLedge = false;
		private float lastLedge = 0f;
		private bool doJump = false;

		void Start() {
			this.rigidbody = GetComponent<Rigidbody>();
			this.collider = GetComponent<CapsuleCollider>();
			this.animator = GetComponent<Animator>();
		}

		void OnDespawn() {
			this.isSpawning = true;
		}

		void OnSpawnStart() {
			this.isSpawning = true;

			this.jumpCount = 0;
			this.isGrounded = true;
			this.transform.eulerAngles = new Vector3(0,180,0);
		}

		void OnSpawnEnd() {
			this.isSpawning = false;
		}

		void OnDodge() {
			this.animator.SetTrigger("Dodge");
		}

		void OnKnockback(Knockback knockback) {
			this.knockback = true;

			this.Invoke("Recover", knockback.power);
		}

		void TriggerJump() {
			this.doJump = true;
		}

		void Recover() {
			this.knockback = false;
		}
		
		void UpdateCommands(CharacterCommandSet commands) {
			this.commands = commands;
		}

		void OnHitGround() {
			this.jumpCount = 0;
		}

		void FixedUpdate() {
			bool isGrounded = CalcIsGrounded();
			if(this.isGrounded != isGrounded) {
				this.isGrounded = isGrounded;

				if(this.isGrounded) {
					SendMessage("OnHitGround",SendMessageOptions.DontRequireReceiver);
				} else {
					SendMessage("OnLeftGround",SendMessageOptions.DontRequireReceiver);
				}
			}

			float deltaTime = Time.fixedDeltaTime;

			UpdateAnimator();
			
			//jump control
			if(this.doJump) {
				if(this.onLedge) {
					GrabLedge(false);
				}
				Jump();
				this.doJump = false;
			//ground control
			} else if(this.isGrounded) {
				this.rigidbody.velocity = this.movement;
			//ledge control
			} else if(this.onLedge) {
				if(this.commands.aimVertical < 0) {
					GrabLedge(false);
				} else if (commands.moveHorizontal > 0 && IsLookingLeft()) {
					GrabLedge(false);
					TurnRight();
				} else if(commands.moveHorizontal < 0 && IsLookingRight()) {
					GrabLedge(false);
					TurnLeft();
				}
			//air control
			} else if(!knockback) {
				this.rigidbody.AddForce(new Vector3(commands.moveHorizontal * this.maxAirControl.x,commands.aimVertical < 0 ? commands.aimVertical * this.maxAirControl.y : 0,0),ForceMode.Acceleration);
			}
		}
		
		void OnAnimatorMove() {
			if(Time.deltaTime <= 0) {
				return;
			}

			Vector3 v = (this.animator.deltaPosition / Time.deltaTime) * this.moveSpeed;
			v.y = 0;

			this.movement = v;
		}

		void OnCollisionStay(Collision collision) {
			if(collision.gameObject.CompareTag("Platform")) {
				/*bool down = false;
				Bounds bounds = this.collider.bounds;
				foreach(ContactPoint point in collision.contacts) {
					Vector3 relative = point.point - bounds.center;
					if(relative.y < -bounds.extents.y + this.collider.radius && Mathf.Abs(relative.x) < bounds.extents.x) {
						down = true;
						break;
					}
				}
				
				if(down) {
					if(!this.isGrounded) {
						//Debug.Log("Hit Ground");
						SendMessage("OnHitGround",SendMessageOptions.RequireReceiver);
					}
					this.isGrounded = true;
				} else */
				if(!this.onLedge && !this.isGrounded && Time.realtimeSinceStartup - lastLedge > 1f) {
					//ledge handling
					Bounds bounds = this.collider.bounds;
					foreach(ContactPoint point in collision.contacts) {
						Vector3 relative = point.point - bounds.center;
						float extentsX = bounds.extents.x;
						if(
							(relative.x > extentsX * 0.9f && transform.eulerAngles.y > 80 && transform.eulerAngles.y < 100) ||
							(relative.x < -extentsX * 0.9f && transform.eulerAngles.y > 260 && transform.eulerAngles.y < 280)
						) {
							float fromX = bounds.center.x + (relative.x > 0 ? extentsX : -extentsX) * 0.9f;
							Vector3 direction = relative.x > 0 ? new Vector3(1,0,0) : new Vector3(-1,0,0);
							float length = extentsX * 0.15f;

							Vector3 headPoint = new Vector3(fromX,bounds.max.y,0);
							Vector3 centerPoint = new Vector3(fromX,bounds.center.y,0);
							Vector3 feetPoint = new Vector3(fromX,bounds.min.y,0);

							bool headHit = CastPlatformRay(headPoint,direction,length);
							bool centerHit = CastPlatformRay(centerPoint,direction,length);
							bool feetHit = CastPlatformRay(feetPoint,direction,length);
							
							//Debug.Log(relative + (headHit ? " head" : "") + (centerHit ? " center" : "") + (feetHit ? " feet" : ""));


							if(!headHit) {
								if(centerHit) {
									Debug.Log("Grab Ledge");

									Vector3 overLedge = headPoint + direction * length;
									Vector3 ledgePoint;
									if(CastPlatformRay(overLedge,new Vector3(0,-1,0),Vector3.Distance(headPoint,centerPoint),out ledgePoint)) {
										Vector3 pos = this.transform.position;

										pos.y = ledgePoint.y - this.collider.bounds.extents.y * 1.8f;

										this.rigidbody.MovePosition(pos);
									}

									GrabLedge(true);
								} else if(feetHit) {
									//climb
								}
							}
							
							break;
						}
					}
				}
			}
		}
		
		/*void OnCollisionExit(Collision collision){
			if(collision.gameObject.CompareTag("Platform")) {
				isGrounded = false;
				SendMessage("OnLeftGround",SendMessageOptions.DontRequireReceiver);
			}
		}*/

		public bool IsGrounded() {
			return this.isGrounded;
		}

		private void UpdateAnimator() {
			
			float forward = 0;
			float vertical = 0;
			bool jump = false;
			
			if(!isSpawning && !knockback) {
				if(!this.onLedge) {
					//turn around
					if (commands.moveHorizontal > 0 && !IsLookingRight()) {
						TurnRight();
					} else if(commands.moveHorizontal < 0 && !IsLookingLeft()) {
						TurnLeft();
					}
					
					//move
					if(IsLookingRight()) {
						forward = commands.moveHorizontal;
					} else if(IsLookingLeft()) {
						forward = -commands.moveHorizontal;
					}
				}
				
				//up & down
				vertical = commands.aimVertical;
				
				//jumping
				if(this.isGrounded || this.jumpCount < this.maxJumpCount) {
					jump = commands.jump;
				}
			}
			
			this.animator.SetBool("InAir",!this.isGrounded);
			this.animator.SetBool("Duck",this.commands.duck);
			this.animator.SetBool("Jump",jump);
			this.animator.SetFloat("Forward",forward);
			this.animator.SetBool("OnLedge",this.onLedge);
			this.animator.SetFloat("VerticalAxis", vertical);
			//this.animator.SetBool("Knockback", knockback);
		}

		private void Jump() {
			
			/*//this.rigidbody.velocity = Vector3.up * this.jumpPower;
			//this.rigidbody.velocity = new Vector3(this.commands.moveHorizontal,0,0);
			//this.rigidbody.velocity = this.rigidbody.velocity.normalized;
			
			Vector3 jumpDir = new Vector3(this.commands.moveHorizontal,1,0).normalized;
			this.rigidbody.AddForce(jumpDir * this.jumpPower,ForceMode.Impulse);*/
			
			
			this.rigidbody.velocity = new Vector3();

			Vector3 jumpForce = new Vector3(this.commands.moveHorizontal * this.jumpPower.x,this.jumpPower.y,0);
			this.rigidbody.AddForce(jumpForce,ForceMode.Impulse);
			
			this.jumpCount++;
		}

		private bool IsLookingLeft() {
			return transform.eulerAngles.y > 260 && transform.eulerAngles.y < 280;
		}

		private bool IsLookingRight() {
			return transform.eulerAngles.y > 80 && transform.eulerAngles.y < 100;
		}

		private void TurnLeft() {
			transform.eulerAngles = new Vector3(0,270,0);
		}

		private void TurnRight() {
			transform.eulerAngles = new Vector3(0,90,0);
		}
		
		private void GrabLedge(bool active) {
			if(Time.realtimeSinceStartup - lastLedge < 1f) {
				return;
			}
			this.onLedge = active;		
			this.rigidbody.useGravity = !active;
			this.rigidbody.velocity = Vector3.zero;
			this.jumpCount = 0;
			if(!active) {
				lastLedge = Time.realtimeSinceStartup;
			}
		}

		private bool CastPlatformRay(Vector3 origin,Vector3 direction,float length) {
			Vector3 point;
			return CastPlatformRay(origin,direction,length,out point);
		}

		private bool CastPlatformRay(Vector3 origin,Vector3 direction,float length,out Vector3 point) {
			RaycastHit[] hits = Physics.RaycastAll(origin,direction,length);
			foreach (RaycastHit hit in hits) {
				if(hit.collider.gameObject.CompareTag("Platform")) {
					point = hit.point;
					return true;
				}
			}

			point = new Vector3();
			return false;
		}
		
		private bool CalcIsGrounded() {
			//raycast
			return CastPlatformRay(this.collider.bounds.center - new Vector3(0,this.collider.bounds.extents.y * 0.9f,0),new Vector3(0,-1,0),this.collider.bounds.extents.y * 0.11f);
			//return Physics.Raycast(this.transform.position, -Vector3.up, this.collider.bounds.extents.y + 0.1);

			//capsule cast
			//collider.bounds are the bounds collider relative to the world. I wanted a 0.1 margin, and 0.18 is the radius of my collider.
			//return Physics.CheckCapsule(collider.bounds.center,new Vector3(collider.bounds.center.x,collider.bounds.min.y-0.1f,collider.bounds.center.z),0.18f));

			//return Physics.CheckCapsule(this.collider.bounds.center,new Vector3(collider.bounds.center.x,collider.bounds.min.y - 0.05f,collider.bounds.center.z), this.collider.radius - 0.05f);
		}
	}	
}