﻿using UnityEngine;
using System.Collections;
using MIGames.IndieSmash.Levels;
using MIGames.IndieSmash.Characters.Util;

namespace MIGames.IndieSmash.Characters {
	[RequireComponent(typeof(Rigidbody))]
	public class RespawnController : MonoBehaviour {
		public GameObject respawnPlatform;
		public float platformSpeed = 1f;
		public float platformLifetime = 2f;

		private new Rigidbody rigidbody;

		private bool alive = false;
		private bool respawning = false;
		private Vector3 currentSpawn;

		void Awake() {
			this.rigidbody = GetComponent<Rigidbody>();

			HidePlatform();
		}

		void OnDespawn() {
			ShowPlatform();

			Level level = Level.FindCurrent();

			//ToDo: calculate respawn start point
			this.transform.position = new Vector3(0,level.boundings.GetSpawnY(),0);

			this.alive = false;
		}

		void OnSpawnStart() {
			this.respawning = true;
			this.alive = true;
			this.GetComponent<Animator>().SetTrigger("Spawn");
		}

		void OnSpawnEnd() {
			//activate physics

			Invoke("HidePlatform",platformLifetime);

			this.respawning = false;
		}

		void UpdateCommands(CharacterCommandSet commands) {
			if(alive && !respawning) {
				if(commands.any) {
					HidePlatform();
				}
			}
		}

		void Update() {
			if(!this.respawning) {
				return;
			}

			//ToDo: calculate respawn end point
			Vector3 respawnEndPos = new Vector3(0,5,0);

			float step = Time.deltaTime * this.platformSpeed;

			this.transform.position = Vector3.MoveTowards(this.transform.position,respawnEndPos,step);
			if(Vector3.Distance(this.transform.position,respawnEndPos) < step) {
				SendMessage("OnSpawnEnd");
			}
		}
		
		void ShowPlatform() {
			this.rigidbody.isKinematic = true;
			this.respawnPlatform.SetActive(true);
		}

		void HidePlatform() {
			this.rigidbody.isKinematic = false;
			this.respawnPlatform.SetActive(false);
		}
	}
}