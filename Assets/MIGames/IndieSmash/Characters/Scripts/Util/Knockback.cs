﻿using UnityEngine;
using System.Collections;

namespace MIGames.IndieSmash.Characters.Util {
	[System.Serializable]
	public class Knockback {
		public float power;
		public Vector3 direction;

		public Knockback(Vector3 direction,float power) {
			this.direction = direction;
			this.power = power;
		}
	}
}