﻿using UnityEngine;
using System.Collections;

namespace MIGames.IndieSmash.Characters.Util {
	[System.Serializable]
	public class CharacterCommandSet {
		public float moveHorizontal;
		public float aimVertical;
		public bool duck;
		public bool physicalAttack;
		public bool specialMove;
		public bool jump;
		public bool drop;

		public bool any {get{return moveHorizontal != 0 || aimVertical != 0 || duck || physicalAttack || specialMove || jump || drop;}}
	}
}