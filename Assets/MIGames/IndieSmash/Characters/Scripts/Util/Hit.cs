﻿using UnityEngine;
using System.Collections;

namespace MIGames.IndieSmash.Characters.Util {
	[System.Serializable]
	public class Hit {
		public int damage;

		public Hit(int damage) {
			this.damage = damage;
		}
	}
}