﻿using UnityEngine;
using System.Collections;

namespace MIGames.IndieSmash.Characters.Actions {
	[System.Serializable]
	public class ActionSet {
		public ActionChain idle;
		public ActionChain forward;
		public ActionChain up;
		public ActionChain down;
		public ActionChain duck;
	}
}