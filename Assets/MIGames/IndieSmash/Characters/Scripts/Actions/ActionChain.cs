﻿using UnityEngine;
using System.Collections;

namespace MIGames.IndieSmash.Characters.Actions {
	[System.Serializable]
	public class ActionChain {
		public CharacterAction[] actions = new CharacterAction[1];
	}
}
