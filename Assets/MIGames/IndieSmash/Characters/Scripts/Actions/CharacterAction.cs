﻿using UnityEngine;
using System.Collections;

namespace MIGames.IndieSmash.Characters.Actions {
	public enum ImpulseMode { None, Impulse } 
	public enum DamageMode { PerHit, Timed }

	[System.Serializable]
	public class CharacterAction {
		public AnimationClip clip;
		
		public ImpulseMode impulseMode;
		public Vector3 impulse;
		
		public DamageMode damageMode;
		public float damageInterval;

		/*public abstract void OnActionStart();
		public abstract void OnActionEnd();
		public abstract void OnActionInterrupt();*/
	}
}