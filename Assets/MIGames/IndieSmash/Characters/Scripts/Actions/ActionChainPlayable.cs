﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Experimental.Director;

namespace MIGames.IndieSmash.Characters.Actions {
	public class ActionChainPlayable : AnimationMixerPlayable {

		private int currentChainIndex = -1;
		private float timeToNextAction;
		private bool nextAction = true;

		public ActionChainPlayable(ActionChain chain) {
			List<AnimationClip> clips = new List<AnimationClip>();
			foreach(CharacterAction action in chain.actions) {
				clips.Add(action.clip);
			}
			this.SetInputs(clips.ToArray());
		}

		public void SetContinue(bool nextAction) {
			this.nextAction = nextAction;
		}

		public override void PrepareFrame(FrameData info) {
			Playable[] inputs = GetInputs();
			
			// Advance to next clip if necessary
			timeToNextAction -= (float)info.deltaTime;
			if(timeToNextAction <= 0.0f) {
				Debug.Log(currentChainIndex + " " + timeToNextAction);
				currentChainIndex++;
				if ((currentChainIndex == 0 || nextAction) && currentChainIndex < inputs.Length) {
					AnimationClipPlayable currentClip = (AnimationClipPlayable)inputs[currentChainIndex];
					
					// Reset the time so that the next clip starts at the correct position
					inputs[currentChainIndex].time = 0;
					timeToNextAction = currentClip.clip.length;
				} else {
					// Pause when queue is complete
					state = PlayState.Paused;
				}
			}
			
			// Adjust the weight of the inputs
			for (int a = 0; a < inputs.Length; a++) {
				if(a == currentChainIndex) {
					SetInputWeight(a, 1.0f);
				} else {
					SetInputWeight(a, 0.0f);
				}
			}
		}
	}
}