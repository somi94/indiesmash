﻿using UnityEngine;
using System.Collections.Generic;
using MIGames.IndieSmash.Characters.Util;
using MIGames.IndieSmash.Characters.Actions;

namespace MIGames.IndieSmash.Characters {
	[RequireComponent(typeof(Animator))]
	[RequireComponent(typeof(MovementController))]
	public class ActionController : MonoBehaviour {
		private Animator animator;
		private MovementController movement;

		public ActionSet primarySet;
		public ActionSet secondarySet;

		private RuntimeAnimatorController animatorController;
		private CharacterCommandSet commands;
		//private string currentTrigger;
		private ActionChainPlayable currentChain;

		void Awake() {
			this.animator = GetComponent<Animator>();
			this.movement = GetComponent<MovementController>();

			this.animatorController = this.animator.runtimeAnimatorController;
		}

		void UpdateCommands(CharacterCommandSet commands) {
			this.commands = commands;

			if(this.currentChain != null) {
				this.currentChain.SetContinue(this.commands.physicalAttack || this.commands.specialMove);
			} else if(CanExecuteAction()) {
				ActionSet set = null;
				if(this.commands.physicalAttack) {
					set = this.primarySet;
				} else if(this.commands.specialMove) {
					set = this.secondarySet;
				}
				if(set != null) {
					ExecuteActionSet(set);
				}
			}
		}

		public bool CanExecuteAction() {
			AnimatorStateInfo info = this.animator.GetCurrentAnimatorStateInfo(0);

			return info.IsTag("Controlable");
		}

		public void ExecuteActionSet(ActionSet set) {
			ActionChain chain = set.idle;
			if(Mathf.Abs(this.commands.moveHorizontal) > 0.5f) {
				chain = set.forward;
			} else if(this.commands.aimVertical > 0.1f) {
				chain = set.up;
			} else if(this.movement.IsGrounded() && this.commands.duck) {
				chain = set.duck;
			} else if(!this.movement.IsGrounded() && this.commands.aimVertical < -0.1f) {
				chain = set.down;
			}

			PlayChain(chain);
		}

		public void PlayChain(ActionChain chain) {
			Debug.Log("play chain");

			this.currentChain = new ActionChainPlayable(chain);
			
			// Bind the queue to the player
			this.animator.Play(this.currentChain);
		}

		/*void UpdateCommands(CharacterCommandSet commands) {
			this.commands = commands;

			this.animator.SetBool("PrimaryAction",commands.physicalAttack);
		}

		void TriggerActionStart(string trigger) {
			//this.currentTrigger = currentTrigger;

			foreach(CharacterAction action in this.actions) {
				if(action.trigger == trigger) {
					this.currentAction = action;
					break;
				}
			}

			if(this.currentAction != null) {
				StartAction(this.currentAction);
			}
		}

		void TriggerActionEnd() {

		}

		void TriggerDodge() {

		}

		private void StartAction(CharacterAction action) {
			if(action.impulseMode != ImpulseMode.None) {
				Rigidbody rigidbody = this.GetComponent<Rigidbody>();
				
				rigidbody.AddForce(action.impulse,ForceMode.VelocityChange);
			}
		}*/
	}	
}
