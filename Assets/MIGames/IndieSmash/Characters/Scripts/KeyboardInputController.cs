﻿using UnityEngine;
using System.Collections;
using MIGames.IndieSmash.Characters.Util;

namespace MIGames.IndieSmash.Characters {
	public class KeyboardInputController : MonoBehaviour {
		void Update() {
			CharacterCommandSet commands = new CharacterCommandSet ();
			commands.moveHorizontal = Input.GetAxis("x-axis");
			commands.aimVertical = Input.GetAxis("y-axis");
			commands.duck = Input.GetAxis("y-axis") < 0;
			commands.physicalAttack = Input.GetButton("action-primary");
			commands.specialMove = Input.GetButton("action-secondary");
			commands.drop = Input.GetButton("drop");
			commands.jump = Input.GetButton("jump");

			this.SendMessage("UpdateCommands",commands);
		}
	}
}
