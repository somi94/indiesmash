﻿using UnityEngine;
using System.Collections;

namespace MIGames.IndieSmash.Characters {
	public class Player : MonoBehaviour {

		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {
		
		}

		public static Player[] FindAll() {
			return GameObject.FindObjectsOfType<Player>();
		}
	}
}