﻿using UnityEngine;
using System.Collections.Generic;
using MIGames.IndieSmash.Characters;
using MIGames.IndieSmash.Camera;

namespace MIGames.IndieSmash.Levels {
	[RequireComponent(typeof(Collider))]
	public class LevelBoundings : MonoBehaviour {
		private RespawnManager respawnManager;
		private CameraController cameraController;
		private new Collider collider;

		// Use this for initialization
		void Start () {
			this.collider = GetComponent<Collider>();
			this.collider.isTrigger = true;

			this.respawnManager = GetComponentInParent<RespawnManager>();
			this.cameraController = GameObject.FindObjectOfType<CameraController>();
		}

		void OnTriggerExit(Collider other) {
			Player player = other.GetComponent<Player>();
			if(player) {
				this.respawnManager.RegisterDespawn(player);
				this.cameraController.Shake(0.5f,0.5f);
			}
		}

		public bool Contains(Vector3 point) {
			return this.collider.bounds.Contains(point);
		}

		public float GetSpawnY() {
			return this.collider.bounds.center.y + this.collider.bounds.extents.y + 3;
		}

		public Player[] GetPlayersInBounds() {
			Player[] players = Player.FindAll();
			List<Player> inBounds = new List<Player>();
			foreach(Player player in players) {
				if(this.Contains(player.GetComponent<Collider>().bounds.center)) {
					inBounds.Add(player);
				}
			}

			return inBounds.ToArray();
		}

		public Bounds GetBounds() {
			return this.collider.bounds;
		}
	}
}
