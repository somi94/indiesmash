﻿using UnityEngine;
using System.Collections;

namespace MIGames.IndieSmash.Levels {
	[ExecuteInEditMode]
	[RequireComponent(typeof(RespawnManager))]
	public class Level : MonoBehaviour {
		public LevelBoundings boundings { get { return _boundings; } }
		private LevelBoundings _boundings;
		public RespawnManager respawn { get { return _respawn; } }
		private RespawnManager _respawn;

		public Material skybox;
		public Color ambience;

		// Use this for initialization
		void Start () {
			RenderSettings.skybox = this.skybox;
			RenderSettings.ambientLight = this.ambience;

			ValidateLevel();
		}
		
		public void ValidateLevel() {
			_respawn = GetComponent<RespawnManager>();

			_boundings = GetComponentInChildren<LevelBoundings>();
			if(_boundings == null) {
				Debug.LogError("level has no boundings...");
			}
		}	

		public static Level FindCurrent() {
			return GameObject.FindObjectOfType<Level>();
		}
	}
}