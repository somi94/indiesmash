﻿using UnityEngine;
using System.Collections.Generic;
using MIGames.IndieSmash.Characters;

namespace MIGames.IndieSmash.Levels {
	public class RespawnManager : MonoBehaviour {
		public float respawnTimer;

		private Dictionary<Player,float> respawns = new Dictionary<Player, float>();

		void Update() {
			Player[] players = new Player[respawns.Keys.Count];
			respawns.Keys.CopyTo(players,0);

			foreach(Player player in players) {
				respawns[player] -= Time.deltaTime;
				if(respawns[player] <= 0) {
					Spawn(player);
					respawns.Remove(player);
				}
			}
		}

		private void Spawn(Player player) {
			player.gameObject.SendMessage("OnSpawnStart");
		}
		
		public void RegisterDespawn(Player player) {
			player.gameObject.SendMessage("OnDespawn");
			
			this.respawns.Add(player, this.respawnTimer);
		}
	}
}
