﻿using UnityEngine;
using System.Collections.Generic;

namespace MIGames.IndieSmash.Levels.Events {
	public class EventController : MonoBehaviour {
		public Event[] events;

		private Dictionary<Event,float> triggers = new Dictionary<Event,float>();

		void Start() {
			foreach(Event ev in events) {
				triggers[ev] = Random.Range(ev.firstTriggerMin,ev.firstTriggerMax);
			}
		}

		void Update() {
			foreach(Event ev in events) {
				//timer is expired, and was not restarted => ignore event
				if(triggers[ev] <= 0) {
					continue;
				}

				//count down timer
				triggers[ev] -= Time.deltaTime;

				//timer is expired 
				if(triggers[ev] <= 0) {
					//trigger event
					this.transform.root.BroadcastMessage("OnLevelEvent",ev.trigger);

					//start new timer, if interval > 0
					if(ev.triggerIntervalMin > 0) {
						triggers[ev] = Random.Range(ev.triggerIntervalMin,ev.triggerIntervalMax);
					}
				}
			}
		}
	}
}
