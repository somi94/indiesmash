﻿using UnityEngine;
using System.Collections;

namespace MIGames.IndieSmash.Levels.Events {
	[System.Serializable]
	public class Event {
		public string trigger;
		public float firstTriggerMin;
		public float firstTriggerMax;
		public float triggerIntervalMin;
		public float triggerIntervalMax;
	}
}