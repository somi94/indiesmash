﻿using UnityEngine;
using System.Collections;
using MIGames.WaypointSystem;

namespace MIGames.IndieSmash.Levels.Events {
	[RequireComponent(typeof(WaypointFollower))]
	public class WaypointFollowerEvent : MonoBehaviour {
		public string trigger;

		void OnLevelEvent(string trigger) {
			if(trigger == this.trigger) {
				GetComponent<WaypointFollower>().StartPath();
			}
		}
	}
}
