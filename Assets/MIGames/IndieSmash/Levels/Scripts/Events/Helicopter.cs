﻿using UnityEngine;
using System.Collections;
using MIGames.WaypointSystem;

namespace MIGames.IndieSmash.Levels.Events {
	[RequireComponent(typeof(WaypointFollowerEvent))]
	public class Helicopter : MonoBehaviour {
		private Animator animator;
		private WaypointFollower follower;

		public float rotatorDampTime = 1f;

		private Waypoint lastWaypoint;

		void Start() {
			this.animator = GetComponentInChildren<Animator>();
			this.follower = GetComponent<WaypointFollower>();
		}

		void Update() {
			float rotator = 0f;
			if(this.follower.IsOnPath() && !this.follower.IsIdling()) {
				rotator = this.follower.CalcCurrentSpeedModifier();
			}

			this.animator.SetFloat("Rotator",rotator,rotatorDampTime,Time.deltaTime);
		}
	}
}
